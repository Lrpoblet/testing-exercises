/// <reference types="Cypress" />

describe('Posts SPA', () => {
  beforeEach(() => {
    cy.visit('http://localhost:8080');
    cy.wait(1000);
  });

  it('user create a new post', () => {
    cy.get('[buttoncontent="Add"] > .button').should('be.visible');
    cy.get('[buttoncontent="Cancel"] > .button').should('be.visible');
    cy.get('#titleInput').type('my new post title');
    cy.get('#bodyInput').type('my new post content');
    cy.get('[buttoncontent="Add"] > .button').click();
    cy.get(':nth-child(1) > post-detail-ui > .postContent').contains(
      'my new post title'
    );
    cy.get('[buttoncontent="Update"] > .button').should('not.be.visible');
    cy.get('[buttoncontent="Delete"] > .button').should('not.be.visible');
  });

  it('user create a new post but cancel', () => {
    cy.get('#titleInput').type('my new post title');
    cy.get('#bodyInput').type('my new post content');
    cy.get('[buttoncontent="Cancel"] > .button').click();
    cy.get('#titleInput').should('have.value', '');
  });

  it('user select post and delete it and button behavior when user select a post', () => {
    cy.get('[buttoncontent="Add"] > .button').should('be.visible');
    cy.get('[buttoncontent="Cancel"] > .button').should('be.visible');
    cy.get(':nth-child(1) > post-detail-ui > .postContent').click();
    cy.get('[buttoncontent="Add"] > .button').should('not.be.visible');
    cy.get('[buttoncontent="Cancel"] > .button').should('be.visible');
    cy.get('[buttoncontent="Update"] > .button').should('be.visible');
    cy.get('[buttoncontent="Delete"] > .button').should('be.visible');
    cy.get('[buttoncontent="Delete"] > .button').click();
    cy.get('[buttoncontent="Add"] > .button').should('be.visible');
    cy.get('[buttoncontent="Cancel"] > .button').should('be.visible');
    cy.get('[buttoncontent="Update"] > .button').should('not.be.visible');
    cy.get('[buttoncontent="Delete"] > .button').should('not.be.visible');
  });

  it('user select post and update it', () => {
    cy.get(':nth-child(2) > post-detail-ui > .postContent').click();
    cy.get('#titleInput').clear();
    cy.get('#titleInput').type('updated title');
    cy.get('#bodyInput').clear();
    cy.get('#bodyInput').type('updated content');
    cy.get('[buttoncontent="Update"] > .button').click();
    cy.get(':nth-child(2) > post-detail-ui > .postContent').contains(
      'updated title'
    );
    cy.get('#titleInput').should('have.value', '');
    cy.get('#bodyInput').should('have.value', '');
  });

  it('user select post and cancel', () => {
    cy.get(':nth-child(2) > post-detail-ui > .postContent').click();
    cy.get('[buttoncontent="Cancel"] > .button').click();
    cy.get('#titleInput').should('have.value', '');
    cy.get('#bodyInput').should('have.value', '');
    cy.get('[buttoncontent="Add"] > .button').should('be.visible');
  });

  it('user select post and come back add mode', () => {
    cy.get(':nth-child(2) > post-detail-ui > .postContent').click();
    cy.get('.addModeBtn').click();
    cy.get('#titleInput').should('have.value', '');
    cy.get('#bodyInput').should('have.value', '');
    cy.get('[buttoncontent="Add"] > .button').should('be.visible');
  });

  it('open posts page', () => {
    cy.get('.nav__link').click();
    cy.url().should('include', '/posts');
    cy.get('[buttoncontent="Back"] > .button').click();
  });
});
