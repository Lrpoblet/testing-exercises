import { LitElement, html } from 'lit';
import { AllPostsUseCase } from '../usecases/all-posts.usecase';
import { AddPostUseCase } from '../usecases/add-post.usecase';
import { UpdatePostUseCase } from '../usecases/update-post-usecase';
import { DeletePostUseCase } from '../usecases/delete-post.usecase';
import '../ui/post-detail.ui';

export class PostsListComponent extends LitElement {
  static get properties() {
    return {
      posts: { type: Array },
    };
  }

  async connectedCallback() {
    super.connectedCallback();
    const storedPosts = localStorage.getItem('posts');
    if (storedPosts) {
      this.posts = JSON.parse(storedPosts);
    } else {
      this.posts = await AllPostsUseCase.execute();
      localStorage.setItem('posts', JSON.stringify(this.posts));
    }
  }

  render() {
    return html`
      <button @click="${this.addViewClick}" class="addModeBtn">Add mode</button>
      <h1 class="title">Posts list</h1>
      <ul>
        ${this.posts?.map(
          (post) =>
            html`<li @click="${() => this.postDetailClick(post)}">
              <post-detail-ui .post=${post}></post-detail-ui>
            </li>`
        )}
      </ul>
    `;
  }

  createRenderRoot() {
    return this;
  }

  addViewClick(e) {
    e.preventDefault();
    this.dispatchEvent(
      new CustomEvent('view:add', {
        bubbles: true,
        composed: true,
      })
    );
  }

  postDetailClick(post) {
    const postSelected = new CustomEvent('post:selected', {
      detail: post,
      bubbles: true,
      composed: true,
    });
    this.dispatchEvent(postSelected);
  }

  async addNewPost(newPost) {
    const updatedPosts = await AddPostUseCase.execute(this.posts, newPost);
    this.posts = updatedPosts;
    localStorage.setItem('posts', JSON.stringify(updatedPosts));
  }

  async deletePost(postId) {
    const updatedPosts = await DeletePostUseCase.execute(this.posts, postId);
    this.posts = updatedPosts;
    localStorage.setItem('posts', JSON.stringify(updatedPosts));
  }

  async updatePost(updatedPost) {
    const updatedPosts = await UpdatePostUseCase.execute(
      this.posts,
      updatedPost
    );
    this.posts = updatedPosts;
    localStorage.setItem('posts', JSON.stringify(updatedPosts));
  }
}

customElements.define('posts-list-component', PostsListComponent);
