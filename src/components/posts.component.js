import { LitElement, html } from 'lit';
import { AllPostsUseCase } from '../usecases/all-posts.usecase';
import '../ui/post.ui';
import '../ui/button.ui';
import { Router } from '@vaadin/router';

export class PostsComponent extends LitElement {
  static get properties() {
    return {
      posts: { type: Array },
    };
  }

  async connectedCallback() {
    super.connectedCallback();
    const storedPosts = localStorage.getItem('posts');
    if (storedPosts) {
      this.posts = JSON.parse(storedPosts);
    } else {
      this.posts = await AllPostsUseCase.execute();
      localStorage.setItem('posts', JSON.stringify(this.posts));
    }
  }

  render() {
    return html`
      <button-ui
        @click="${this.goHome}"
        type="submit"
        buttonContent="Back"
      ></button-ui>
      <ul>
        ${this.posts?.map(
          (post) =>
            html`<li class="postContainer">
              <post-ui .post=${post}></post-ui>
            </li>`
        )}
      </ul>
    `;
  }

  goHome() {
    Router.go('/');
  }

  createRenderRoot() {
    return this;
  }
}

customElements.define('posts-component', PostsComponent);
