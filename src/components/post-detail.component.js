import { LitElement, html } from 'lit';
import '../ui/post-detail.ui';

export class DetailPostComponent extends LitElement {
  connectedCallback() {
    super.connectedCallback();
    this.posts = [];
    this.titleValue = '';
    this.contentValue = '';
    this.postId = '';
  }

  static get properties() {
    return {
      posts: { type: Array },
      titleValue: { type: String },
      contentValue: { type: String },
      postId: { type: Number },
    };
  }

  render() {
    return html`
      <section class="formSection">
        <h2 class="title">Post detail</h2>
        <form action="" @submit="${this.submitForm}" class="form">
          <label class="form__label"
            >Title<input
              class="form__input"
              type="text"
              id="titleInput"
              @change="${this.setTitleInput}"
              .value="${this.titleValue}"
              placeholder="Add post title here"
              size="50"
          /></label>
          <label class="form__label"
            >Body<input
              class="form__input"
              type="text"
              id="bodyInput"
              @change="${this.setContentInput}"
              .value="${this.contentValue}"
              placeholder="Add post content here"
              size="50"
          /></label>
          <div class="form__buttons">
            <button-ui
              type="reset"
              @click="${this.addClick}"
              ?hidden="${this.postId}"
              buttonContent="Add"
            >
            </button-ui>
            <button-ui
              type="reset"
              @click="${this.cancelClick}"
              buttonContent="Cancel"
            ></button-ui>
            <button-ui
              type="submit"
              @click="${this.updateClick}"
              ?hidden="${!this.postId}"
              buttonContent="Update"
            >
            </button-ui>
            <button-ui
              type="submit"
              @click="${this.deleteClick}"
              ?hidden="${!this.postId}"
              buttonContent="Delete"
            >
            </button-ui>
          </div>
        </form>
      </section>
    `;
  }

  //handle post select values
  handleSelectedPost(post) {
    this.titleValue = post.title;
    this.contentValue = post.content;
    this.postId = post.id;
  }

  setTitleInput(e) {
    this.titleValue = e.target.value;
  }
  setContentInput(e) {
    this.contentValue = e.target.value;
  }

  reset() {
    this.titleValue = '';
    this.contentValue = '';
    this.postId = '';
  }

  submitForm() {
    e.preventDefault();
  }

  //add button from posts-list-component
  addButton() {
    this.reset();
  }

  //buttons events
  cancelClick(e) {
    e.preventDefault();
    this.reset();
  }

  addClick(e) {
    e.preventDefault();
    const newPost = {
      content: this.contentValue,
      title: this.titleValue,
    };
    const addPost = new CustomEvent('post:add', {
      bubbles: true,
      composed: true,
      detail: newPost,
    });
    this.dispatchEvent(addPost);
    this.reset();
  }

  deleteClick(e) {
    e.preventDefault();

    const deletePost = new CustomEvent('post:delete', {
      bubbles: true,
      composed: true,
      detail: this.postId,
    });
    this.dispatchEvent(deletePost);
    this.reset();
  }

  updateClick(e) {
    e.preventDefault();
    const updatedPost = {
      id: this.postId,
      content: this.contentValue,
      title: this.titleValue,
    };

    const updatePost = new CustomEvent('post:update', {
      bubbles: true,
      composed: true,
      detail: updatedPost,
    });
    this.dispatchEvent(updatePost);
    this.reset();
  }

  createRenderRoot() {
    return this;
  }
}

customElements.define('post-detail-component', DetailPostComponent);
