import '../components/posts.component';

export class PostPage extends HTMLElement {
  connectedCallback() {
    this.innerHTML = `<posts-component></posts-component>`;
  }
}
customElements.define('posts-page', PostPage);
