import '../components/posts-list.component';
import '../components/post-detail.component';

export class HomePage extends HTMLElement {
  constructor() {
    super();
  }

  getStyles() {
    return `
    <style>
      home-page {
        width: 100%;
        height: 100%;
        display: grid;
        grid-template-columns: 1fr 2fr;
        grid-gap: 1rem;
      }
    </style>
    `;
  }

  connectedCallback() {
    this.innerHTML = `${this.getStyles()}
    <posts-list-component id="postList"></posts-list-component>
    <post-detail-component id="postDetail"></post-detail-component>
    `;

    const postList = this.querySelector('#postList');
    const postDetail = this.querySelector('#postDetail');

    this.addEventListener('post:selected', (e) => {
      postDetail.handleSelectedPost(e.detail);
    });

    this.addEventListener('post:add', (e) => {
      postList.addNewPost(e.detail);
    });
    this.addEventListener('post:delete', (e) => {
      postList.deletePost(e.detail);
    });
    this.addEventListener('post:update', (e) => {
      postList.updatePost(e.detail);
    });
    this.addEventListener('view:add', () => {
      postDetail.addButton();
    });
  }
}
customElements.define('home-page', HomePage);
