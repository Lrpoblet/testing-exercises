import { Post } from '../model/post';
import { PostsRepository } from '../repositories/posts.repository';

export class AddPostUseCase {
  static async execute(posts = [], postModel) {
    const repository = new PostsRepository();
    const newPostAPI = await repository.addNewPost(postModel);

    const newPostModel = new Post({
      id: newPostAPI.id,
      title: newPostAPI.title,
      content: newPostAPI.body,
    });

    return [newPostModel, ...posts];
  }
}
