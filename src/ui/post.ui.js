import { LitElement, html } from 'lit';
import './../components/posts.component';

export class PostUI extends LitElement {
  static get properties() {
    return {
      post: { type: Object },
    };
  }

  render() {
    return html`<h2 class="postTitle">${this.post?.title}</h2>
      <p class="postContent">${this.post?.content}</p>`;
  }

  createRenderRoot() {
    return this;
  }
}
customElements.define('post-ui', PostUI);
