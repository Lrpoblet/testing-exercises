import { LitElement, html } from 'lit';
import '../components/posts-list.component';

export class PostDetailUI extends LitElement {
  static get properties() {
    return {
      post: { type: Object },
    };
  }

  render() {
    return html`<p class="postContent list">${this.post?.title}</p> `;
  }

  createRenderRoot() {
    return this;
  }
}
customElements.define('post-detail-ui', PostDetailUI);
