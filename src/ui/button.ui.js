import { LitElement, html } from 'lit';

export class ButtonUI extends LitElement {
  static get properties() {
    return {
      buttonContent: { type: String },
    };
  }

  render() {
    return html`
      <button id="button-${this.buttonContent}" class="button">
        ${this.buttonContent}
      </button>
    `;
  }

  createRenderRoot() {
    return this;
  }
}

customElements.define('button-ui', ButtonUI);
