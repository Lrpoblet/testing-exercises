import { PostsRepository } from '../src/repositories/posts.repository';
import { AddPostUseCase } from '../src/usecases/add-post.usecase';

jest.mock('../src/repositories/posts.repository');

describe('Add new post use case', () => {
  beforeEach(() => {
    PostsRepository.mockClear();
  });

  it('should add new post', async () => {
    const POSTS = [
      {
        id: 1,
        title:
          'sunt aut facere repellat provident occaecati excepturi optio reprehenderit',
        content:
          'quia et suscipit\nsuscipit recusandae consequuntur expedita et cum\nreprehenderit molestiae ut ut quas totam\nnostrum rerum est autem sunt rem eveniet architecto',
      },
      {
        id: 2,
        title: 'qui est esse',
        content:
          'est rerum tempore vitae\nsequi sint nihil reprehenderit dolor beatae ea dolores neque\nfugiat blanditiis voluptate porro vel nihil molestiae ut reiciendis\nqui aperiam non debitis possimus qui neque nisi nulla',
      },
    ];

    const newPost = {
      content: 'New post content',
      title: 'New post title',
    };

    PostsRepository.mockImplementation(() => {
      return {
        addNewPost: () => {
          return {
            id: 101,
            title: newPost.title,
            body: newPost.content,
            userId: 1,
          };
        },
      };
    });

    const postAdded = await AddPostUseCase.execute(POSTS, newPost);

    expect(postAdded.length).toBe(POSTS.length + 1);
    expect(postAdded[POSTS.length - 1].id).toBeDefined();
    expect(postAdded[0].title).toBe('New post title');
  });
});
