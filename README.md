# Posts SPA

## Author

Lara R. Poblet - [@lrpoblet](https://gitlab.com/lrpoblet)

## Abstract

Is a Simple Page Application built with Lit Element that pulls posts from an external API and uses add, delete and update methods to update the list of posts.

## Getting Started

### Installation

Clone the repo:

`git clone https://gitlab.com/Lrpoblet/testing-exercises`

Install NPM packages

`npm install`

### Usage

To run the app you just need to use the following command:

`npm run start`
